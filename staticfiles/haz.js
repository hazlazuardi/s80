var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
    acc[i].addEventListener("click", function() {
        this.classList.toggle("active");
        var panel = this.nextElementSibling;
        if (panel.style.display === "block") {
            panel.style.display = "none";
        } else {
            panel.style.display = "block";
        }
    });
}

$(function() {
    $('.up').on('click', function(e) {
        var wrapper = $(this).closest('li')
        wrapper.insertBefore(wrapper.prev())
    })
    $('.down').on('click', function(e) {
        var wrapper = $(this).closest('li')
        wrapper.insertAfter(wrapper.next())
    })
})