$(function() {
    $('.up').on('click', function(e) {
        var wrapper = $(this).closest('li')
        wrapper.insertBefore(wrapper.prev())
    })
    $('.down').on('click', function(e) {
        var wrapper = $(this).closest('li')
        wrapper.insertAfter(wrapper.next())
    })
})


$(".accordion").on('click', function() {
    var parentContainer = $(this).closest('li')
    if ($(parentContainer).find(".panel").css("display") === "none") {
        $(parentContainer).find(".panel").css("display", "block")
    } else {
        $(parentContainer).find(".panel").css("display", "none")
    }
})