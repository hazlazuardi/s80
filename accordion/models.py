from django.db import models

# Create your models here.

class Bio(models.Model):
    title = models.CharField(max_length=26)
    content = models.TextField()
    pub_date = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ('-pub_date',)
        verbose_name_plural = 'Bios'

    def __str__(self):
        return self.title
    objects = models.Manager()