from django.test import TestCase

from django.test import Client
from django.urls import resolve

from .views import index
from .models import Bio

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

# Create your tests here.

class S80UnitTest(TestCase):

    def test_url_index_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
    
    def test_views_index_func_used(self):
        found = resolve('/')
        self.assertEqual(found.func, index)
    
    def test_template_index_used(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'landing.html')

    def test_model_create_new_content(self):
        Bio.objects.create(
            title='About Me', 
            content='Jadi begiiniiiiiii'
            )
        queryset = Bio.objects.all()
        content_count = Bio.objects.all().count()
        self.assertEqual(content_count, 1)


    def test_two_entries(self):
        Bio.objects.create(title='1-title', content='1-body')
        Bio.objects.create(title='2-title', content='2-body')
        response = self.client.get('/')
        self.assertContains(response, '1-title')
        self.assertContains(response, '1-body')
        self.assertContains(response, '2-title')

    # coverage run --include='accordion/*' manage.py test 

class S80FunctionalTest(TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')

        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(S80FunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(S80FunctionalTest, self).tearDown()

    def test_input_message(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('http://story8haz.herokuapp.com/')
        # selenium.get('http://127.0.0.1:8000/')
        
        selenium.implicitly_wait(60)

        # find the form element
        up = selenium.find_element_by_css_selector('a.up')
        down = selenium.find_element_by_css_selector('a.down')

        up.click()
        down.click()
