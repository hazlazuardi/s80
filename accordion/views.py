from django.shortcuts import render

from .models import Bio

# Create your views here.

def index(request):
    queryset = Bio.objects.all()
    context = {
        'queryset':queryset,
    }
    return render(request, 'landing.html', context)